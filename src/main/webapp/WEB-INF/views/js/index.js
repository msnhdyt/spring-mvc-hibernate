console.log("hello!")

function removeButton(elementButton){
    const id = elementButton.parentElement.getAttribute("id");
    const a = document.getElementsByTagName('a');
    console.log(a)
    let url = a[0].getAttribute("context-name");
    url += "/mhs/delete?id=" + id;
    fetch(url).then((response) => console.log(response));
    alert("successfully deleted, please refresh the page!")
}

document.addEventListener("DOMContentLoaded", (event)=>{
    const deleteElement = document.querySelectorAll(".delete-btn");
    deleteElement.forEach((btn) => {
        btn.addEventListener("click", (event)=>{
            removeButton(event.target)
        })
    })
})