<!DOCTYPE web-app PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd" >
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<html>
<head>
	<title>Home</title>
	<style type="text/css">
    <%@include file="css/style.css" %></style>
</head>
<body>
	<header>
		<h2>Welcome to Spring MVC Tutorial</h2>	
	</header>
	<main>
		<ul>
			<li><a href="mhs">Mahasiswa</a></li>
		</ul>
	</main>
</body>
</html>
