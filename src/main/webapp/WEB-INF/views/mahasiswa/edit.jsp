<!DOCTYPE web-app PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd" >
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName",request.getContextPath());
%>
<html>
<head>
	<title>Edit</title>
	<style type="text/css">
    <%@include file="../css/style.css" %></style>
	<!-- <style>
		@import url('https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap');
		*{
		    box-sizing: border-box;
		}
		header{
			padding: 10px;
			background-color: #141a35;
			color: white;
		}
		body{
		    font-family: "Quicksand", sans-serif;
		    margin: 0;
		    padding: 0;
		    /* background-color: #6998AB; */
		    background-color: #84DFFF;
		    color: black;
		}
		main {
			display: flex;
			flex-direction: row;
		}
		.flex-item{
			flex-grow: 1;
		    /* border: 1px solid black; */
		}
		form{
		    /* background-color: rgb(136, 133, 133); */
		    margin: 20px;
		    padding: 20px;		    
		}
		.form-group{
		    display: flex;
		    flex-direction: column;
		}
		#list-mhs{
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
			border: 1px solid black;
		    border-radius: 5px;
		    padding: 20px;
		
		    overflow: auto;
		    
		    height: 560px;
		    background-color: whitesmoke;
		    margin-right: 20px;
		    margin-top: 20px;
		    min-width: 560px;
		}
		input {
			font-family: Raleway;
		   	border: 2px solid black;
		   	border-radius: 8px;
		   	padding: 8px;
		  	box-sizing: border-box;
		   	margin-bottom: 8px;
		   	font-size: 20px;
		}
		.submit-btn:hover{
			cursor: pointer;
		}
		.item-mhs{
		    border: 1px solid black;
		    margin: 5px;
		    padding: 5px;
		    border-radius: 8px;
		}
		.item-mhs h4{
			margin-top: 1px;
		}
		.item-mhs:hover{
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
		}
		.item-mhs h4, p4{
			padding: 2px;
		}
		.item-mhs p{
			margin: 0;
		}
		.edit-btn{
			float: right;
			position: relative;
			bottom: 20px;		
			cursor: pointer;	
		}
	</style> -->
</head>
<body>
	<header> 
		<h2>Welcome to Spring MVC Tutorial</h2>
	</header>
	
	<main>
		<form action="${contextName}/mhs" method="post" class="flex-item">
		<input type="hidden" name="mode" value="edit">
		<input type="hidden" name="id" value="${mahasiswa.id}">
		<%-- NIM <input type="text" name="nim" value="${mahasiswa.nim}"><br>
		Nama <input type="text" name="nama" value="${mahasiswa.nama}"><br>
		Alamat <input type="text" name="alamat" value="${mahasiswa.alamat}"><br>
		Tanggal Lahir <input type="date" name="dob" value="${mahasiswa.dob}"><br>
		<button type="submit">Simpan</button> --%>
		<div class="form-group">
				<label for="nim">NIM</label>
				<input type="text" name="nim" value="${mahasiswa.nim}" id="nim">
			</div>
			<div class="form-group">
				<label for="nama">Nama</label>
				<input type="text" name="nama" value="${mahasiswa.nama}" id="nama">
			</div>
			<div class="form-group">
				<label for="dob">Alamat</label>
				<input type="text" name="alamat" value="${mahasiswa.alamat}" id="alamat">
			</div>
			<div class="form-group">
				<label for="dob">Tanggal Lahir</label>
				<input type="date" name="dob" value="${mahasiswa.dob}" id="dob">
			</div>
			<button type="submit" class="submit-btn">Simpan</button>
		</form>
		
		<div id="list-mhs" class="flex-item">
			<c:forEach var="mhs" items="${mhslist}">
			<article class="item-mhs">
				<h4>${mhs.nama}</h4>
				<p> ${mhs.nim} </p>
				<p> ${mhs.alamat} </p>
				<!-- <button class="delete-btn">Delete</button> -->
				<a href="${contextName}/mhs/edit?id=${mhs.id}"><button class="edit-btn">Edit</button></a>
			</article>
			<%-- <li>${mhs.nim} - ${mhs.nama} - <a href="${contextName}/mhs/edit?id=${mhs.id}">Edit</a></li> --%>
			</c:forEach>	
		</div>
	</main>
	<footer>
		<p> footer </p>
	</footer>
	<%-- <script type="text/javascript"> <%@include file="../js/index.js" %></script> --%>
</body>
</html>
